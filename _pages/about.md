---
layout: about
title: about
permalink: /
subtitle: <a href="https://www.innovamics.com">Innovamics</a>. CEO & Founder.

profile:
  align: right
  image: prof_pic.jpg
  image_circular: false # crops the image to make it circular
  address: >
    <p>Innovamics</p>
    <p>87 Avenue Freres Perret</p>
    <p>69003, Lyon, France</p>

news: true  # includes a list of news items
selected_papers: true # includes a list of papers marked as "selected={true}"
social: false # includes social icons at the bottom of the page
---

Hi, thanks for stopping by, 

I am passionate about applied science, technology and innovation. I have a PhD
in mechanics, with application in computational mechanics. I am specialized in
nonlinear mechanics modeling of continuous solids. For the last 8 years I have
been working intensively on modeling and simulation of composite materials.

In 2019, I created the company Innovamics in order to create a new concept in
the simulation market.

I am currently developing an open program for modeling composite materials. The
goal is to create a collaborative work platform, to accelerate the
industrialization of research.

Beyond work, I am passionate about programming, science fiction, cycling and
post-rock.