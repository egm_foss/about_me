---
layout: page
permalink: /publications/
title: publications
description: >
    My research work since 2016. <br>
    Main subjects: Composite forming, Continuum Mechanics, Computational Mechanics, Finite Elements. 
years: [2022, 2021, 2019, 2018, 2017, 2016]
nav: true
nav_order: 1
---
<!-- _pages/publications.md -->
<div class="publications">

{%- for y in page.years %}
  <h2 class="year">{{y}}</h2>
  {% bibliography -f papers -q @*[year={{y}}]* %}
{% endfor %}

</div>
